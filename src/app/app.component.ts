import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from './services/data.service';
import { AgGridAngular } from 'ag-grid-angular';
import { CreateimageComponent } from './pages/createimage/createimage.component';
import { PagelinksComponent } from './pages/pagelinks/pagelinks.component';
import { DatePipe } from '@angular/common';
import 'ag-grid-enterprise'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('agGrid', {static: false}) agGrid: AgGridAngular;
  title = 'aggridapp';
  flag:boolean = false;
  selectedNodes:any=[];
  checkToggle = true;
  sideBar:any ={
    toolPanels: [
      {
        id: "columns",
        labelDefault: "Columns",
        labelKey: "columns",
        iconKey: "columns",
        toolPanel: "agColumnsToolPanel",
        toolPanelParams: {
          toolPanelSuppressRowGroups: true,
          toolPanelSuppressValues: true,
          toolPanelSuppressPivots: true,
          toolPanelSuppressPivotMode: true
        }
      }
    ]
  }
  ;


  getContextMenuItems(params) {
    console.log(params);
    if(params.column.colId != 'title'){
      return false;
    }
    var result = [
      {
        name: "Open in new Tab ",
        action: function() {
          let url = 'https://www.youtube.com/watch?v=' + params.value;
          // window.alert("Youtube URL " + params.value);
          window.open(url,'_blank');
        },
        tooltip: "Click here to open Youtube Video in new tab",
        cssClasses: ["redFont", "bold"]
      },      
      "copy"
    ];
    return result;
  }


 
  rowData: any = [];
  columnDefs = [
    {
      
      headerName: '', field: 'thumbnail',sortable: true 
    ,checkboxSelection: this.checkToggle
     ,headerCheckboxSelection: this.checkToggle
     ,cellRendererFramework: CreateimageComponent
    },
    {headerName: 'Published on'
    ,field: 'published' 
    ,sortable: true
   
  },
    {headerName: 'Video Title', field: 'title',sortable: true ,cellRendererFramework: PagelinksComponent},
    {headerName: 'Description', field: 'description',sortable: true}
];
constructor(private service:DataService,private datePipe: DatePipe) {
 
}

toggleColumnDef(){
  this.checkToggle = !this.checkToggle;
  this.columnDefs = [
      {
        headerName: '', field: 'thumbnail',sortable: true 
      ,checkboxSelection: this.checkToggle
      ,headerCheckboxSelection: this.checkToggle
      ,cellRendererFramework: CreateimageComponent
      },
      {headerName: 'Published on'
      ,field: 'published' 
      ,sortable: true
    
    },
      {headerName: 'Video Title', field: 'title',sortable: true ,cellRendererFramework: PagelinksComponent},
      {headerName: 'Description', field: 'description',sortable: true}
  ];
  this.flag = !this.flag;
  setTimeout(() => {
      this.flag = !this.flag;

  }, 100);
}
getRowHeight(params) {
  return 100;
};
ngOnInit() {
      this.service.datacalling().then((res:any)=>{
        console.log(res);
        res.items.map((item,index)=>{
          console.log(index); 
          let data = {
            thumbnail:item.snippet.thumbnails.default ,
            published:this.datePipe.transform(new Date(item.snippet.publishedAt),'yyyy-MM-dd , hh:mm:ss a'),
            title:item.id.videoId,
            description:item.snippet.description,
            fieldtitle:item.snippet.title
          }
          if(index == 49){
            this.flag = true;
          }
          this.rowData.push(data);
         })
         console.log(this.rowData);
      });
  }
onSelectionChanged() {
  var selectedRows = this.agGrid.api.getSelectedRows();
  this.selectedNodes = selectedRows;
}
getMainMenuItems(params) {
  switch (params.column.getId()) {
    case "thumbnail":
      var athleteMenuItems = params.defaultItems.slice(0);
      athleteMenuItems.push({
        name: "ag-Grid Is Great",
        action: function() {
          console.log("ag-Grid is great was selected");
        }
      });
      athleteMenuItems.push({
        name: "Casio Watch",
        action: function() {
          console.log("People who wear casio watches are cool");
        }
      });
      athleteMenuItems.push({
        name: "Custom Sub Menu",
        subMenu: [
          {
            name: "Black",
            action: function() {
              console.log("Black was pressed");
            }
          },
          {
            name: "White",
            action: function() {
              console.log("White was pressed");
            }
          },
          {
            name: "Grey",
            action: function() {
              console.log("Grey was pressed");
            }
          }
        ]
      });
      return athleteMenuItems;
    case "age":
      return [
        {
          name: "Joe Abercrombie",
          action: function() {
            console.log("He wrote a book");
          },
          icon: '<img src="../images/lab.png" style="width: 14px;"/>'
        },
        {
          name: "Larsson",
          action: function() {
            console.log("He also wrote a book");
          },
          checked: true
        },
        "resetColumns"
      ];
    case "country":
      var countryMenuItems = [];
      var itemsToExclude = ["separator", "pinSubMenu", "valueAggSubMenu"];
      params.defaultItems.forEach(function(item) {
        if (itemsToExclude.indexOf(item) < 0) {
          countryMenuItems.push(item);
        }
      });
      return countryMenuItems;
    default:
      return params.defaultItems;
  }
}

}
